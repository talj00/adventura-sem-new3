package logika;

/**
 *  Třída PrikazDej implementuje pro hru příkaz dej.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazDej implements IPrikaz {
    private static final String NAZEV = "dej";
    private HerniPlan plan;
    private Batoh batoh;
    private Hra hra;

    /**
     *  Konstruktor třídy
     *  
     *  
     */    
    public PrikazDej(Batoh batoh, HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.batoh = batoh;
        this.hra = hra;
    }

    /**
     *  Provádí příkaz "dej".Uskuteční výmněnu mezi postavou a osobou. Osoba musí být v místnosti a
     *                          musí chtít věc.
     *
     *@param parametry - jako  parametr 1 obsahuje jméno postavy,
     *                         se kterou se má provést výmněna.
     *                         parametr 2 obsahuje jméno věci,
     *                         která se má vymněnit.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length == 0) {
            return "Nevim, co mám dát.";
        }
        if (parametry.length == 1) {
            return "Příkaz musí mít právě dva parametry \"osobu\" a \"věc\"";
        }
        if (parametry.length >= 3) {
            return "Příliš mnoho parametrů, příkaz musí mít právě dva parametry \"osobu\" a \"věc\"";
        }           
        Prostor kdeJsme = plan.getAktualniProstor();
        String vecCoDa = parametry[1];
        String osobaKomu = parametry[0];
        Osoba osubka = kdeJsme.vyberOsoba(osobaKomu);
        if (osubka == null){
            return "Tato osoba zde není";}
        Vec vecicka = batoh.vemVec(vecCoDa);
        if (vecicka == null){
            return "Tato věc v batohu není";}
        if (vecicka.equals(osubka.getCoChce())){
            Vec coDostane = osubka.getCoDostane();
            batoh.setVec(coDostane);
            batoh.vyberVec(vecCoDa);
            osubka.probehla(true);
            return osubka.getChci()+"\n Z batohu bylo vyndáno "+vecicka.getNazev()+" a dáno "+coDostane.getNazev()+".";
        }
        return osubka.getNechci();
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
