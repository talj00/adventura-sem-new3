package logika;

/**
 *  *  Třída PrikazPouzij implementuje pro hru příkaz pouzij.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazPouzij implements IPrikaz {
    private static final String NAZEV = "pouzij";
    private HerniPlan plan;
    private Batoh batoh;


    /**
     *  Konstruktor třídy
     *  
     *  
     */    
    public PrikazPouzij(HerniPlan plan, Batoh batoh) {
        this.plan = plan;
        this.batoh = batoh;
    }

    /**
     *  Provádí příkaz "pouzij". Použije věc, která musí být v batohu a zároveň musí být použitelná
     *                          v dané místnosti.
     *
     *@param parametry - jako  parametr obsahuje jméno věci,
     *                         která se má použít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    public String proved(String... parametry) {
        if (parametry.length == 0) {
            return "Nevim, co mám pužít.";
        }
        Prostor kdeJsme = plan.getAktualniProstor();
        String vecCo = parametry[0];
        Vec vecicka = batoh.vemVec(vecCo);
        if (vecicka == null){
            return "Tato věc v batohu není";}

        else {
            return "To nemohu zde na nic použít."; }

    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    public String getNazev() {
        return NAZEV;
    }

}
