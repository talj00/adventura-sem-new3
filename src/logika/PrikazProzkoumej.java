package logika;

/**
 *  Třída PrikazProzkoumej implementuje pro hru příkaz prozkoumej.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazProzkoumej implements IPrikaz {
    private static final String NAZEV = "prozkoumej";
    private HerniPlan plan;
    /**
     *  Konstruktor třídy
     *  
     *  
     */    
    public PrikazProzkoumej(HerniPlan plan) {
        this.plan = plan;

    }

    /**
     *  Provádí příkaz "prozkoumej". Prozkoumá danou věc zda-li v ní není další schovaná věc.
     *
     *@param parametry - jako  parametr obsahuje jméno věci,
     *                         která se má prozkoumat.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length != 1) {
            // pokud chybí druhé slovo (sousední místnost), tak se vypíše obsah místnosti a východy z ní
            return plan.getAktualniProstor().seznamVeci()+plan.getAktualniProstor().seznamOsob();
        }   
        String coJe = parametry[0];
        Prostor kdeJsme = plan.getAktualniProstor();
        Vec vecicka = kdeJsme.vemVec(coJe);
        if (vecicka==null) {
            return "Taková věc zde není";
        }
        vecicka.prozkoumano(true);
        return vecicka.popisProzkoumej();
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
